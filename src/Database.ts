
const container = new Map<string,any[]>();
import contains from "is-subset";
import merge from "deepmerge";
import clone from "clone-deep";


export class Database{

    private index=1;
    constructor(private name:string){

        if(!container.has(name)){
            container.set(name,[]);
        }
    }

    private get storage(){
        return container.get(this.name) as any[];
    }

    insert<K extends Partial<{id:number}>>(object:K){
        object.id=this.index++;

        this.storage.push(clone(object));

    }

    private findInternal(query:{}){
        return this.storage.filter(item=>contains(item,query))

    }

    async find(query:{}){
        return this.findInternal(query).map(clone);
    }


    get(query){
        return (this.find(query)[0])
    }

    async update(query:{},update:{}){
        let current = this.findInternal(query)
        current.forEach(item=>{
            let updated = merge(item,update);
            let index = this.storage.indexOf(item);
            this.storage.splice(index,1,updated);
        })
    }


    async remove(query:{}){
        let result = this.findInternal(query);
        result.map(item=>{
            let index = this.storage.indexOf(item)
            this.storage.splice(index,1);
        })
    }




}